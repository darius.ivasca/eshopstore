# eShopStore


## Precondiții înainte de utilizarea aplicației web eShopStore
- Versiunea Java 17 instalată.
- Disponibilitatea unui mediu de integrare precum **IntelliJ IDEA**, **Spring Tools bazat pe Eclipse** sau **Spring Tools bazat pe Visual Studio Code**. **ATENTIE!** Întrucat Visual Studio Code este un editor de text este nevoie de instalarea unor extensii puse la dispoziție pe marketplace **(Spring Boot Extension Pack)**. 
- MySQL instalat.


## Utilizarea aplicației eShopStore
In **MySQL** trebuie importată baza de date pentru a se putea realiza conexiunea între aplicație și aceasta și pentru ca aplicația să poată rula fără probleme. Pentru a face acest lucru trebuiesc îndepliniți următorii pași:
1. În meniul de sus este opțiunea **"Server"** și **"Data Import"**.
2. Se va deschide o fereastră, acolo va trebui aleasă opțiunea **"Import from Self-Contained File** și va trebui ales fișierul SQL disponibil în arhiva aplicației.
3. Va trebui aleasă baza de date unde se vor importa datele, unde se poate alege fie **"Default Target Schema"** fie va trebui creată o nouă bază de date. Baza de date implicită se numește **webappdb**.
4. Odată realizați acești pași, se va putea selecta opțiunea **"Start Import"**.

***Aplicația în acest moment, din punct de vedere al bazei de date, este pregătită pentru utilizare.***

Ulterior, va trebui importat proiectul fie într-un mediu de integrare fie într-un editor de text. Va trebui mai întâi **dezarhivată** arhiva ce conține codul sursă al aplicației.

## Actualizarea fișierelor de configurare a aplicației eShopStore
În fișierele **application.properties** din proiectul _WebAppBackend_ și **application.yml** din _WebAppFrontend_, vor trebui schimbate credențialele necesare pentru accesul asupra bazei de date MySQL.

**Din acest moment aplicația este pregătită să fie rulată.**



